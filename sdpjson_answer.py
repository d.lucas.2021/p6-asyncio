import asyncio
import sdp_transform
import json

class SDPJSONAnswerProtocol:
    def __init__(self, on_con_lost):
        self.on_con_lost = on_con_lost
        self.transport = None

    def connection_made(self, transport):
        print("Initalizing connection")
        self.transport = transport

    def datagram_received(self, data, addr):
        json_data = json.loads(data.decode())
        print('Received:',(json_data))

        sdp_str = json_data["sdp"]
        sdp_data = sdp_transform.parse(sdp_str)

        sdp_data['media'][0]['port'] = 34543
        sdp_data['media'][1]['port'] = 34543

        print('Send:', sdp_data)
        sdp_str_modified = sdp_transform.write(sdp_data)
        self.transport.sendto(sdp_str_modified.encode(), addr)

    def connection_lost(self, exc):
        print('Connection closed')
        self.on_con_lost.set_result(True)

async def main():
    loop = asyncio.get_running_loop()

    on_con_lost = loop.create_future()

    transport, protocol = await loop.create_datagram_endpoint(
        lambda: SDPJSONAnswerProtocol(on_con_lost),
        local_addr=('127.0.0.1', 9993))

    try:
        await on_con_lost
    finally:
        transport.close()

asyncio.run(main())
