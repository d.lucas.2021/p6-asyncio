import asyncio

async def counter(id, time): #contador
    print('Counter', id, ':starting')
    for i in range(1,time+1):
        await asyncio.sleep(1)
        print("Counter",id,":",i)
    print('Counter', id, ':finishing')

async def main():
    await asyncio.gather(counter("A", 4), counter("B", 2), counter("C", 6))

asyncio.run(main())
